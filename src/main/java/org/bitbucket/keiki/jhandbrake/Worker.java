package org.bitbucket.keiki.jhandbrake;

import static org.bitbucket.keiki.jhandbrake.Configuration.COMMAND_NAME;
import static org.bitbucket.keiki.jhandbrake.Configuration.DELETE_SOURCE;
import static org.bitbucket.keiki.jhandbrake.Configuration.NICE_LEVEL;
import static org.bitbucket.keiki.jhandbrake.Configuration.PRESET;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Worker implements Runnable {

   private static final Logger logger  = LogManager.getLogger();
   private volatile boolean    running = true;
   private Instant lastTaskTaken;


   @Override
   public void run() {
      Optional<String> next;
      QueueStorage queueStorage = QueueStorage.getInstance();
      while ( keepOnRunning() && (next = queueStorage.getNext()).isPresent() ) {
         next.
               ifPresentOrElse(s -> {
                  lastTaskTaken = Instant.now();
                  processRecord(queueStorage, s);
               }, () -> logger.info("Nothing more to process."));
      }
      running = false;
   }

   public Instant getLastTaskTaken() {
      return lastTaskTaken;
   }

   private boolean keepOnRunning() {
      if ( !running ) {
         logger.info("Stopping worker.");
      }
      return running;
   }

   private void processRecord( QueueStorage queueStorage, String s ) {
      logger.info("next is {}", s);
      Path file = Path.of(s);
      int returnCode = process(file);

      if ( returnCode == 0 ) {
         deleteSourceAfterFinished(file);
         queueStorage.complete(s);
      } else {
         logger.warn("Not zero return code {} for file {}", returnCode, s);
      }
   }


   private static void deleteSourceAfterFinished( Path file ) {
      if ( DELETE_SOURCE ) {
         try {
            Files.delete(file);
         }
         catch ( IOException e ) {
            logger.warn("Could not delete source file after completion {}", file);
         }
      }
   }

   private static int process( Path path ) {
      try {
         String pathString = path.toString();
         int indexOfExt = pathString.lastIndexOf(".");
         String s1 = pathString.substring(0, indexOfExt) + ".mkv";
         ProcessBuilder builder = new ProcessBuilder();
         builder.command( "nice", "-n", NICE_LEVEL, COMMAND_NAME, "-i", path.toString(), "-o", s1, "--preset",   PRESET);
         builder.directory(new File(System.getProperty("user.home")));
         Process process = builder.start();
         InputStream inputStream = process.getInputStream();
         try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {

            while ( (bufferedReader.readLine()) != null ) {
            }
         }
         OutputStream outputStream = process.getOutputStream();
         outputStream.close();

         return process.waitFor();
      }
      catch ( Exception e ) {
         e.printStackTrace();
         return -1;
      }
   }

   public void stop() {
      running = false;
      logger.info("This worker will stop after current task");
   }

   public boolean isRunning() {
      return running;
   }
}
