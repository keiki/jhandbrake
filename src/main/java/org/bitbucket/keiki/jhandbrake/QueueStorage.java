package org.bitbucket.keiki.jhandbrake;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class QueueStorage {

   private static final QueueStorage INSTANCE = new QueueStorage();

   private static final Logger logger         = LogManager.getLogger();
   private static final String FILE_PATH_NAME = System.getProperty("user.home") + "/.jhandbrake/queue";
   private static final Path FILE_PATH      = Path.of(FILE_PATH_NAME);
   private static final Charset     CHARSET = StandardCharsets.UTF_8;
   private static final Set<String> inProgress = new HashSet<>();

   private QueueStorage() {
   }

   public static QueueStorage getInstance() {
      ensureQueueFileExists();

      return INSTANCE;
   }

   private static void ensureQueueFileExists() {
      try {
         if (Files.notExists(FILE_PATH.getParent())) {
            Files.createDirectories(FILE_PATH.getParent());
         }
         if (Files.notExists(FILE_PATH)) {
            Files.createFile(FILE_PATH);
         }
      } catch (IOException e) {
      }
   }

   public synchronized void add( List<String> newItemsInQueue ) {
      List<String> strings = readAllFiles();
      strings.addAll(newItemsInQueue);
      store(strings);
   }

   public synchronized Optional<String> getNext() {
      List<String> fileNames = readAllFiles();
      if ( fileNames.isEmpty() ) {
         logger.info("Nothing more to do");
         return Optional.empty();
      }
      int size = fileNames.size();
      size = size - inProgress.size() - 1;
      logger.info("Taking one. {} left to process in queue", size);

      for ( String fileName : fileNames ) {
         if ( inProgress.contains(fileName) ) {
            continue;
         }
         inProgress.add(fileName);
         return Optional.of(fileName);
      }
      logger.info("Worker has nothingNot more todo.");
      return Optional.empty();
   }

   private List<String> readAllFiles() {
      try {
         return Files.readAllLines(Path.of(FILE_PATH_NAME), CHARSET);
      }
      catch ( IOException e ) {
         throw new UncheckedIOException(e);
      }
   }

   public synchronized void complete(String fileName) {
      inProgress.remove(fileName);
      List<String> fileNames = readAllFiles();
      List<String> listWithoutFile = fileNames.stream().filter(s -> !s.equals(fileName)).collect(Collectors.toList());
      store(listWithoutFile);
   }

   private void store( List<String> items ) {
      try {
         String strings = String.join("\n", items);
         Files.writeString(FILE_PATH, strings, CHARSET);
      }
      catch ( IOException e ) {
         throw new UncheckedIOException(e);
      }
   }
}
