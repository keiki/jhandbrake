package org.bitbucket.keiki.jhandbrake;

import java.util.concurrent.TimeUnit;


public class Helper {
   public static void sleepForASec() {
      try {
         TimeUnit.SECONDS.sleep(1);
      }
      catch ( InterruptedException e ) {
         e.printStackTrace();
      }
   }

}
