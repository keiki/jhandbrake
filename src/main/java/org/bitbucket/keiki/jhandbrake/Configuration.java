package org.bitbucket.keiki.jhandbrake;

import java.nio.file.FileVisitOption;


public class Configuration {
   public static final String            COMMAND_NAME       = "HandBrakeCLI";
   public static final String            PRESET             = "Fast 1080p30";
//   public static final String            PRESET             = "H.265 MKV 720p30";
   public static final String            EXTENSION          = ".mkv";
   public static final boolean           DELETE_SOURCE      = true;
   public static final String            NICE_LEVEL         = "19";
   public static final int               MAX_DEPTH            = 30;
   public static int                     initialNumberThreads = 3;
   public static final FileVisitOption[] FILE_VISIT_OPTIONS   = new FileVisitOption[]{ FileVisitOption.FOLLOW_LINKS};
  public static final String            HOME_DIR           = System.getProperty("user.home");

}
