package org.bitbucket.keiki.jhandbrake;

import static org.bitbucket.keiki.jhandbrake.Configuration.DELETE_SOURCE;
import static org.bitbucket.keiki.jhandbrake.Configuration.EXTENSION;
import static org.bitbucket.keiki.jhandbrake.Configuration.FILE_VISIT_OPTIONS;
import static org.bitbucket.keiki.jhandbrake.Configuration.HOME_DIR;
import static org.bitbucket.keiki.jhandbrake.Configuration.MAX_DEPTH;
import static org.bitbucket.keiki.jhandbrake.Configuration.initialNumberThreads;
import static org.bitbucket.keiki.jhandbrake.Helper.sleepForASec;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Main {

   private static Logger logger = LogManager.getLogger();
   private static final String WORKER_CONFIG_CHANGE_FILE = "/worker";

   public static void main( String[] args ) throws IOException, InterruptedException {
      if ( args.length ==2 && args[0].equalsIgnoreCase("-a") ) {
         addToQueue(args[1]);
      } else {
         if (args.length==2 && args[0].equalsIgnoreCase("-n") ) {
            initialNumberThreads = Integer.parseInt(args[1]);
         }
         processAllInQueue();
      }
   }

   private static void addToQueue( String arg ) throws IOException {
      Path parentPath = Paths.get(arg);
      Stream<Path> directoryStream = Files.find(parentPath, MAX_DEPTH,
            ( path1, basicFileAttributes ) -> basicFileAttributes.isRegularFile() && StringUtils.endsWithIgnoreCase(path1.toString(), EXTENSION),
            FILE_VISIT_OPTIONS);
      List<String> newItemsInQueue = directoryStream.map(Path::toAbsolutePath)
            .map(Path::toString)
            .sorted(Comparator.naturalOrder())
            .collect(Collectors.toList());

      QueueStorage queueStorage = QueueStorage.getInstance();
      queueStorage.add(newItemsInQueue);
   }
   private static void printWarningForDeletionOfSource() {
      if ( DELETE_SOURCE ) {
         logger.warn("WARNING: Source file will be deleted after successful encoding! You have 5 seconds to abort with ctrl + c");
         for ( int i = 5; i > 0; i-- ) {
            logger.info(i + " seconds until start");
            sleepForASec();
         }
      }
      if ( stopFileExists() ) {
         logger.warn("Stop file exists!");
      }
      if ( shutdownFileExists() ) {
         logger.warn("Shutdown file exists");
      }
   }

   private static void processAllInQueue() throws InterruptedException {
      printWarningForDeletionOfSource();

      checkFilesExists();

      executorService = Executors.newCachedThreadPool();

      increaseWorkerBy(initialNumberThreads);

      do {
         boolean stop = stopFileExists();
         boolean shutdown = shutdownFileExists();

         if ( runtimeConfigChange() ) {
            int targetWorkerCount = fetchTargetNumberOfWorker();
            int currentWorkerCount = workers.size();
            if ( targetWorkerCount > currentWorkerCount ) {
               int workersToCreate = targetWorkerCount - currentWorkerCount;
               logger.info("Create {} new worker", workersToCreate);
               increaseWorkerBy(workersToCreate);
            } else if ( targetWorkerCount < currentWorkerCount ) {
               decreaseWorkerBy(currentWorkerCount - targetWorkerCount);
            }
         }

         if ( stop || shutdown ) {
            workers.forEach(Worker::stop);
            executorService.shutdown();
            executorService.awaitTermination(2, TimeUnit.DAYS);
            if ( stop ) {
               logger.info("Stop file exists. Stop!");
               deleteFileInHomeDirectoryBeforeExit("/stop");
               return;
            }
            if ( shutdown ) {
               shutdownComputer();
            }
         }
         TimeUnit.SECONDS.sleep(20);
      }
      while ( workers.stream().anyMatch(Worker::isRunning) );
      if ( shutdownCompleteFileExists() ) {
         shutdownComputer();
      }

   }

   private static void createAndStartWorker() {
      Worker worker = new Worker();
      workers.add(worker);
      executorService.submit(worker);
   }

   private static void decreaseWorkerBy( int i ) {
      //add stop to worker is current task is running the longest.
      workers.sort(Comparator.comparing(Worker::getLastTaskTaken));
      for ( int j = 0; j < i; j++ ) {
         workers.get(j).stop();
         workers.remove(j);
      }
   }

   private static void increaseWorkerBy( int workersToCreate ) {
      for ( int i = 0; i < workersToCreate; i++ ) {
         createAndStartWorker();
      }
   }

   private static int fetchTargetNumberOfWorker() {
      try {
         return Integer.parseInt(Files.readString(Path.of(HOME_DIR + WORKER_CONFIG_CHANGE_FILE), StandardCharsets.UTF_8).trim());
      }
      catch ( IOException | NumberFormatException e ) {
         logger.warn("Can't read new number of workers", e);
         return workers.size();
      }
   }

   private static boolean runtimeConfigChange() {
      return fileExistsInHomeDirectory(WORKER_CONFIG_CHANGE_FILE);
   }

   private static void checkFilesExists() {
      if ( shutdownCompleteFileExists() ) {
         logger.info("shutdownComplete file exists. Please remove before start");
      }
      if ( shutdownFileExists() ) {
         logger.info("shutdown file exists. Please remove before start");
      }
      if ( stopFileExists() ) {
         logger.info("stop file exists. Please remove before start");
      }
   }

   private static boolean shutdownCompleteFileExists() {
      return fileExistsInHomeDirectory("/shutdownAtEnd");
   }

   private static boolean shutdownFileExists() {
      return fileExistsInHomeDirectory("/shutdown");
   }

   private static boolean fileExistsInHomeDirectory( String s ) {
      return Files.exists(Path.of(HOME_DIR + s));
   }

   private static boolean stopFileExists() {
      return fileExistsInHomeDirectory("/stop");
   }


   private static void deleteFileInHomeDirectoryBeforeExit(String fileName) {
      try {
         Path file = Path.of(HOME_DIR + fileName);
         Files.delete(file);
      }
      catch ( IOException e ) {
         logger.info("Couldn't delete file {}",fileName,e );
      }
   }

   private static void shutdownComputer() {
      logger.info("initiate shutdown in 10 sec!");
      for ( int i = 10; i > 0; i-- ) {
         logger.info("Shutting down in {} seconds", i);
         sleepForASec();
      }
      try {
         Runtime.getRuntime().exec("shutdown -h now");
         System.exit(0);
      }
      catch ( IOException e ) {
         System.exit(-1);
      }
   }

   private static ExecutorService executorService;
   private static ArrayList<Worker> workers = new ArrayList<>();
}
